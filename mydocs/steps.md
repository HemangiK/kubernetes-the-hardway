# Steps

- install virtualbox, vagrant and vagrant-hosts

- install client tools like cfssl, cfssljson and kubectl

- `vagrant up` to start the virtual machines, during this cmd execution, this executes the deployment script heartbeat.sh and uses heartbeat to create VIP: 192.168.100.100 on the three controller nodes.

- Create the CA configuration file and Create the CA certificate signing request, then Generate the CA certificate and private key.

- Create the admin client certificate signing request, then Generate the admin client certificate and private key.

- Generate a certificate and private key for each Kubernetes worker node.

- Generate the kube-proxy client certificate and private key.

- Generate the Kubernetes API Server certificate and private key.

- Generate a kubeconfig file for each worker node.

- Generate a kubeconfig file for the kube-proxy service.

- Generate an encryption key and an encryption config suitable for encrypting Kubernetes Secrets.

- Install the etcd server and the etcdctl command line utility in each controller nodes, then start the etcd server.

- Install the Kubernetes Controller Binaries like kubectl, kube-apiserver, kube-controller-manager, kube-scheduler in each controller nodes.

- Create systemd service unit files for kube-apiserver, kube-controller-manager, kube-scheduler; and start the controller services in each controller nodes.

- Create the system:kube-apiserver-to-kubelet ClusterRole with permissions to access the Kubelet API and perform most common tasks associated with managing pods.

- Bind the system:kube-apiserver-to-kubelet ClusterRole to the kubernetes user.

- Install the OS dependencies on each worker instances.

- Create the installation directories and Install the worker binaries on each worker instances.

- Create systemd service unit files for Kubelet, Kubernetes Proxy, cri-containerd; then start the worker services in each worker instances.

- Generate a kubeconfig file suitable for authenticating as the admin user.

- Use flannel to solve communication problems between pods.

- Deploy the kube-dns cluster add-on, which provides DNS based service discovery to applications running inside the Kubernetes cluster.
