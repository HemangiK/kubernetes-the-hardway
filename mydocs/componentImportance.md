# Component Importance

## cfssl and cfssljson

- Cloudflare's PKI and TLS toolkit will be used to provision a PKI Infrastructure and generate TLS certificates.

## kubectl

- It is command line utility for running commands against Kubernetes clusters and used to interact with the Kubernetes API Server.

## virtual machines

- It is used to host the Kubernetes control plane and the worker nodes where containers are ultimately run.

## kubeconfigs

- Kubernetes configuration files, which enable Kubernetes clients to locate and authenticate to the Kubernetes API Servers.

## Node Authorizer

- The Node authorizer allows a kubelet to perform API operations. This includes:

  - Read operations:

    services
    endpoints
    nodes
    pods
    secrets, configmaps, persistent volume claims and persistent volumes related to pods bound to the kubelet’s node

  - Write operations:

    nodes and node status (enable the NodeRestriction admission plugin to limit a kubelet to modify its own node)
    pods and pod status (enable the NodeRestriction admission plugin to limit a kubelet to modify pods bound to itself)
    events

  - Auth-related operations:

    read/write access to the certificationsigningrequests API for TLS bootstrapping
    the ability to create tokenreviews and subjectaccessreviews for delegated authentication/authorization checks

## etcd

- It is a server to store cluster state.

## kube-apiserver

- The Kubernetes API server validates and configures data for the api objects which include pods, services, replicationcontrollers, and others. The API Server services REST operations and provides the frontend to the cluster’s shared state through which all other components interact.

## kubelet

- The kubelet is the primary “node agent” that runs on each node. The kubelet works in terms of a PodSpec. A PodSpec is a YAML or JSON object that describes a pod. The kubelet takes a set of PodSpecs that are provided through various mechanisms (primarily through the apiserver) and ensures that the containers described in those PodSpecs are running and healthy. The kubelet doesn’t manage containers which were not created by Kubernetes.

## kube-proxy

- The Kubernetes network proxy runs on each node. This reflects services as defined in the Kubernetes API on each node and can do simple TCP and UDP stream forwarding or round robin TCP and UDP forwarding across a set of backends.

## kube-scheduler

- The Kubernetes scheduler is a policy-rich, topology-aware, workload-specific function that significantly impacts availability, performance, and capacity. The scheduler needs to take into account individual and collective resource requirements, quality of service requirements, hardware/software/policy constraints, affinity and anti-affinity specifications, data locality, inter-workload interference, deadlines, and so on. Workload-specific requirements will be exposed through the API as necessary.

## kube-controller-manager

- The Kubernetes controller manager is a daemon that embeds the core control loops shipped with Kubernetes. In applications of robotics and automation, a control loop is a non-terminating loop that regulates the state of the system. In Kubernetes, a controller is a control loop that watches the shared state of the cluster through the apiserver and makes changes attempting to move the current state towards the desired state. Examples of controllers that ship with Kubernetes today are the replication controller, endpoints controller, namespace controller, and serviceaccounts controller.

Next: [Steps in summary](mydocs/steps.md)
