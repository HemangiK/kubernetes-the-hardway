## Steps

### Step 1 Prerequisites

- VirtualBox
- vagrant: to manage virtual machine resources
- vagrant-host plugin: to manage the /etc/hosts file in the virtual machine

### Step 2 Installing the Client Tools

- Install following command line utilities:
  - cfssl: will be used to provision a PKI Infrastructure and generate TLS certificates
  - cfssljson: will be used to provision a PKI Infrastructure and generate TLS certificates
  - kubectl: used to interact with the Kubernetes API Server

### Step 3 Provisioning Compute Resources

- Kubernetes requires a set of machines to host the Kubernetes control plane and the worker nodes where containers are ultimately run. In this lab you will provision the compute resources required for running a secure and highly available Kubernetes cluster.

- When Vagrant creates a virtual machine, it executes the deployment script heartbeat.sh and uses heartbeat to create VIP: 192.168.100.100 on the three controller nodes. This IP is used for external access to Kubernetes.

### Step 4 Provisioning a CA and Generating TLS Certificates

- In this lab you will provision a PKI Infrastructure using CloudFlare's PKI toolkit, cfssl, then use it to bootstrap a Certificate Authority, and generate TLS certificates for the following components: etcd, kube-apiserver, kubelet, and kube-proxy.

- Certificate Authority: used to generate additional TLS certificates

- The Kubelet Client Certificates: Kubernetes uses a special-purpose authorization mode called Node Authorizer, that specifically authorizes API requests made by Kubelets. In order to be authorized by the Node Authorizer, Kubelets must use a credential that identifies them as being in the system:nodes group, with a username of system:node:<nodeName>. In this section you will create a certificate for each Kubernetes worker node that meets the Node Authorizer requirements.

- The Kubernetes API Server Certificate: The kubernetes-the-hard-way static IP address will be included in the list of subject alternative names for the Kubernetes API Server certificate. This will ensure the certificate can be validated by remote clients.

### Step 5 Generating Kubernetes Configuration Files for Authentication

- In this lab you will generate Kubernetes configuration files, also known as kubeconfigs, which enable Kubernetes clients to locate and authenticate to the Kubernetes API Servers.

- Client Authentication Configs : will generate kubeconfig files for the kubelet and kube-proxy clients

### Step 6 Generating the Data Encryption Config and Key

- Kubernetes stores a variety of data including cluster state, application configurations, and secrets. Kubernetes supports the ability to encrypt cluster data at rest.

- In this lab you will generate an encryption key and an encryption config suitable for encrypting Kubernetes Secrets.

### Step 7 Bootstrapping the etcd Cluster

- Kubernetes components are stateless and store cluster state in etcd. In this lab you will bootstrap a three node etcd cluster and configure it for high availability and secure remote access.

### Step 8 Bootstrapping the Kubernetes Control Plane

- In this lab you will bootstrap the Kubernetes control plane across three compute instances and configure it for high availability. The following components will be installed on each node: Kubernetes API Server, Scheduler, and Controller Manager.

- RBAC for Kubelet Authorization: In this section you will configure RBAC permissions to allow the Kubernetes API Server to access the Kubelet API on each worker node. Access to the Kubelet API is required for retrieving metrics, logs, and executing commands in pods.

### Step 9 Bootstrapping the Kubernetes Worker Nodes

- In this lab you will bootstrap three Kubernetes worker nodes. The following components will be installed on each node: runc, container networking plugins, cri-containerd, kubelet, and kube-proxy.

### Step 10 Configuring kubectl for Remote Access

- In this lab you will generate a kubeconfig file for the kubectl command line utility based on the admin user credentials.

- Each kubeconfig requires a Kubernetes API Server to connect to. To support high availability the VIP will be used.

### Step 11 Provisioning Pod Network Routes

- Flannel: Use flannel to solve communication problems between pods.

### Step 12 Deploying the DNS Cluster Add-on

- In this lab you will deploy the DNS add-on which provides DNS based service discovery to applications running inside the Kubernetes cluster.

### Step 13 Smoke Test

- In this lab you will complete a series of tasks to ensure your Kubernetes cluster is functioning correctly.

- Data Encryption

- Deployments

- Port Forwarding

- Logs

- Exec

- Services

### Step 14 Cleaning Up

- Delete the controller and worker compute instances created during this tutorial

Next: [Importance Of Components](componentImportance.md)
